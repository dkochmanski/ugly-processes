
(defpackage #:ugly-processes/test
  (use #:cl #:5am #:ugly-processes))

(in-package #:ugly-processes/test)

(defsuite :ugly-processes-tests
    :description "Tests for the ugly processes.")

;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: UGLY-PROCESSES; -*-
;;;   Title: Library for replacing MP deps in the old libraries
;;; Created: 2016-01-17
;;;  Author: Daniel Kochmański <daniel@turtleware.eu>
;;; License: LGPL-2.1
;;; ----------------------------------------------------------------------
;;; (c) copyright 2001 by Gilbert Baumann
;;; (c) copyright 2002 by John Wiseman (jjwiseman@yahoo.com)
;;; (c) copyright 2003 by Daniel Barlow <dan@metacircles.com>
;;; (c) copyright 2016 by Daniel Kochmański <daniel@turtleware.eu>

;;; Library is heavily inspired (hence the copyrights) by SBCL backend
;;; for CLIM-2 and ACL-COMPAT (with regard to emulating things like
;;; state, whostate etc). Using it is highly discouraged unless you
;;; are replacing implementation-specific hacks for multiprocessing in
;;; some old libraries (like McCLIM and ACL-COMPAT) with more portable
;;; BORDEAUX-THREADS.

(in-package #:ugly-processes)

(defstruct (process (:constructor %make-process)
                    (:predicate processp))
  name
  state
  whostate
  function                       ; function which will be run
  arguments                      ; arguments to the function
  thread                         ; thread itself or nil
  %lock                          ; lock for process structure mutators
  run-reasons                    ; primitive mailbox for IPC
  arrest-reasons                 ;
  %queue                         ; queue for condition-wait
  initial-bindings               ; special variable bindings
  property-list)

(defun make-current-process ()
  "Wraps the current thread in our process structure."
  (%make-process
   :name (bt:thread-name (bt:current-thread))
   :function nil
   :thread (bt:current-thread)))

(defparameter *current-process* (make-current-process)
  "Contains the current process (ie REPL), it's a programmer
responsibility to not kill it.")

(defparameter *all-processes* (list *current-process*)
  "A list of processes created by this library, plus the one that was
running when this file was loaded.")

(defparameter *all-processes-lock*
  (bt:make-lock "Lock around *ALL-PROCESSES*")
  "Useful for operations on the *ALL-PROCESSES*.")

(defvar *permanent-queue*
  (bt:make-lock "Lock for disabled threads")
  "We implement DISABLE-PROCESS by making the disablee attempt to lock
the *PERMANENT-QUEUE* which is already locked because we locked it
here. ENABLE-PROCESS just interrupts the lock attempt.")

(handler-case (bt:acquire-lock *permanent-queue* nil)
  (simple-error () nil))

(defun reinit-processes ()
  "Kills all processes (except the *CURRENT-THREAD*) and
re-initializes the *ALL-PROCESSES* list, which is returned."
  (loop :for p :in *all-processes* :do
     (unless (eql p *current-process*)
       (process-kill (process-thread p))))
  (setf *current-process* (make-current-process))
  (bt:with-lock-held (*all-processes-lock*)
    (setf *all-processes* (list *current-process*)))
  *all-processes*)



(defconstant *multiprocessing-p* bt:*supports-threads-p*
  "The value of *multiprocessing-p* is t if the current Lisp
environment supports multi-processing, otherwise it is nil.")

(defun make-process (&key (name "Anonymous")
                       reset-action run-reasons arrest-reasons
                       resume-hook suspend-hook initial-bindings)
  "Creates a process named NAME. Process isn't started yet."
  (declare (ignore reset-action resume-hook suspend-hook ))
  (let ((p (%make-process
            :name name
            :run-reasons run-reasons
            :arrest-reasons arrest-reasons
            :initial-bindings initial-bindings
            :%lock (bt:make-lock
                    (format nil "Internal lock for ~A" name))
            :%queue (bt:make-condition-variable
                     :name (format nil "Blocking queue for ~A" name)))))
    (bt:with-lock-held (*all-processes-lock*)
      (push p *all-processes*))
    p))

;;; This is how CLIM2 defines DESTROY-PROCESS
(defun process-kill (process)
  "Terminates the process process. process is an object returned by
make-process."
  (when (process-thread process)
    (bt:destroy-thread (process-thread process))
    (setf (process-thread process) nil))
  (bt:with-lock-held (*all-processes-lock*)
    (setf *all-processes* (delete process *all-processes*))))

(defun current-process ()
  "Returns the currently running process, which will be the same kind
of object as would be returned by make-process. "
  *current-process*)

(defun process-name-to-process (name)
  (bt:with-lock-held (*all-processes-lock*)
    (find name *all-processes* :key #'process-name :test #'equal)))

(defun all-processes ()
  "Returns the currently running process, which will be the same kind
of object as would be returned by make-process."
  (copy-list *all-processes*))

(defun process-wait (reason redicate &rest arguments)
  "Causes the current process to wait until PREDICATE returns
true. REASON is a reason for waiting, usually a string. On systems
that do not support multi-processing, process-wait will loop until
predicate returns true."
  (declare (type function predicate))
  (let ((old-state (process-whostate *current-process*)))
    (unwind-protect
         (progn
           (setf old-state (process-whostate *current-process*)
                 (process-whostate *current-process*) reason)
           (loop
              (let ((it (apply predicate arguments)))
                (when it (return it)))
              (process-yield)))
      (setf (process-whostate *current-process*) old-state))))

(defun process-wait-with-timeout (reason timeout predicate)
  "Causes the current process to wait until either predicate returns
true, or the number of seconds specified by timeout has
elapsed. reason is a \"reason\" for waiting, usually a string. On
systems that do not support multi-processing,
process-wait-with-timeout will loop until predicate returns true or
the timeout has elapsed."
  (declare (type function predicate))
  (let ((old-state (process-whostate *current-process*))
        (end-time (+ (get-universal-time) timeout)))
    (unwind-protect
         (progn
           (setf old-state (process-whostate *current-process*)
                 (process-whostate *current-process*) reason)
           (loop
              (let ((it (funcall predicate)))
                (when (or (> (get-universal-time) end-time) it)
                  (return it)))
              (process-yield)))
      (setf (process-whostate *current-process*) old-state))))

(defun process-yield ()
  "Allows other processes to run. On systems that do not support
multi-processing, this does nothing."
  (bt:thread-yield))

(defun process-interrupt (process function)
  "Interrupts the process process and causes it to evaluate the
function function. On systems that do not support multi-processing,
this is equivalent to funcall'ing function."
  (bt:interrupt-thread (process-thread process) function))

(defun disable-process (process)
  "Disables the process process from becoming runnable until it is
enabled again."
  (bt:interrupt-thread
   (process-thread process)
   (lambda ()
     (catch 'interrupted-wait (bt:with-lock-held (*permanent-queue*))))))

(defun enable-process (process)
  "Allows the process process to become runnable again after it has
been disabled."
  (bt:interrupt-thread
   (process-thread process)
   (lambda () (throw 'interrupted-wait nil))))

(defun restart-process (process)
  "Restarts the process process by \"unwinding\" it to its initial
state, and reinvoking its top-level function."
  (labels ((boing ()
             (let ((*current-process* process)
                   (bindings (process-initial-bindings process))
                   (function (process-function process))
                   (arguments (process-arguments process)))
               (declare (type function function))
               (if bindings
                   (progv
                       (mapcar #'car bindings)
                       (mapcar #'(lambda (binding)
                                   (eval (cdr binding)))
                               bindings)
                     (apply function arguments))
                   (apply function arguments)))))
    (when (process-thread process)
      (bt:destroy-thread (process-thread process)))
    ;; XXX handle run-reasons in some way?  Should a process continue
    ;; running if all run reasons are taken away before
    ;; restart-process is called?  (process-revoke-run-reason handles
    ;; this, so let's say (setf (process-run-reasons process) nil) is
    ;; not guaranteed to do the Right Thing.)
    (when (setf (process-thread process)
                (bt:make-thread #'boing :name (process-name process)))
      process)))

(defun process-revoke-run-reason (process object)
  "Removes the run reason and disabled the process if there is no more
run reasons left."
  (bt:with-recursive-lock-held ((process-%lock process))
    (prog1
        (setf (process-run-reasons process)
              (delete object (process-run-reasons process)))
      (when (and (process-thread process)
                 (not (process-run-reasons process)))
        (disable-process process)))))

(defun process-add-run-reason (process object)
  (bt:with-recursive-lock-held ((process-%lock process))
    (prog1
        (push object (process-run-reasons process))
      (if (and (process-thread process)
               (not (process-arrest-reasons process)))
          (enable-process process)
          (restart-process process)))))

(defun process-revoke-arrest-reason (process object)
  (bt:with-recursive-lock-held ((process-%lock process))
    (prog1
        (setf (process-arrest-reasons process)
              (delete object (process-arrest-reasons process)))
      (if (and (process-thread process)
               (process-run-reasons process)
               (not (process-arrest-reasons process)))
          (enable-process process)
          (restart-process process)))))

(defun process-add-arrest-reason (process object)
  (bt:with-recursive-lock-held ((process-%lock process))
    (prog1
        (push object (process-arrest-reasons process))
      (when (process-thread process)
        (disable-process process)))))

;;; CLIM-2 (make-process (function &key name) ...)
(defun process-run-function (name-or-options preset-function
                             &rest preset-arguments)
  "Creates a process named NAME. The new process will evaluate the
function PRESET-FUNCTION. On systems that do not support
multi-processing, make-process will signal an error."
  (let* ((make-process-args (etypecase name-or-options
                              (list name-or-options)
                              (string (list :name name-or-options))))
         (process (apply #'make-process make-process-args)))
    (apply #'process-preset process preset-function preset-arguments)
    (setf (process-run-reasons process) :enable)
    (restart-process process)
    process))

(defun process-preset (process function &rest arguments)
  (setf (process-function process) function
        (process-arguments process) arguments)
  (when (process-thread process) (restart-process process)))

;;; CLIM-2
(defun process-active-p (process)
  (bt:thread-alive-p (process-thread process)))


;;; Locks

;;; ACL-COMPAT, CLIM-2 (make-lock (&optional name) ...)
(defun make-process-lock (&key name)
  (bt:make-lock name))

(defun process-lock (lock &optional lock-value whostate timeout)
  (declare (ignore lock-value whostate timeout))
  (bt:acquire-lock lock))

(defun process-unlock (lock &optional lock-value)
  (declare (ignore lock-value))
  (bt:release-lock lock))

;;; CLIM-2
(defun make-recursive-lock (&optional name)
  "Creates a recursive lock whose name is name.

On systems that do not support locking, this will return a new list of
one element, nil. A recursive lock differs from an ordinary lock in
that a process that already holds the recursive lock can call
with-recursive-lock-held on the same lock without blocking."
  (bt:make-recursive-lock name))

;;; CLIM-2
(defmacro with-recursive-lock-held ((place &optional state) &body body)
  "Evaluates body with the recursive lock named by place. place is a
reference to a recursive lock created by make-recursive-lock.

On systems that do not support locking, with-recursive-lock-held is
equivalent to progn."
  (declare (ignore state))
  `(bt:with-recursive-lock-held (,place) ,@body))

(defmacro with-process-lock ((place &key timeout whostate norecursive)
			     &body body)
  (declare (ignore timeout))
  (let ((old-whostate (gensym "OLD-WHOSTATE")))
    `(,(if (null norecursive)
           'bt:with-recursive-lock-held
           'bt:with-lock-held)
       (,place)
       (let (,old-whostate)
         (unwind-protect
              (progn
                (when ,whostate
                  (setf ,old-whostate
                        (process-whostate *current-process*))
                  (setf (process-whostate *current-process*)
                        ,whostate))
                ,@body)
           (setf (process-whostate *current-process*)
                 ,old-whostate))))))

(defmacro with-timeout ((seconds &body timeout-forms) &body body)
  "Evaluates BODY with the time constraint. If the timeout SECONDS is
reached, then TIMEOUT-FORMS are evaluated."
  `(handler-case
       (bt:with-timeout (,seconds) ,@body)
     (bt:timeout (&key length)
       (declare (ignore length))
       ,@timeout-forms)))


;;; Atomic constructs are disabled on all platforms for a sake of
;;; consistency. They are not portable nor possible to implement on
;;; the SMP systems.

(defmacro without-scheduling (&body body)
  "Evaluates BODY in a context that is guaranteed to be free from
interruption by other processes. On systems that do not support
multi-processing, without-scheduling is equivalent to progn."
  (declare (ignore body))
  (error "WITHOUT-SCHEDULING isn't supported."))

(defmacro without-interrupts (&body body)
  "Evaluates BODY in a context that is guaranteed to be free from
interruption by other processes. On systems that do not support
multi-processing, without-interrupts is equivalent to progn."
  (declare (ignore body))
  (error "WITHOUT-SCHEDULING isn't supported."))

(defun atomic-incf (reference)
  "Increments the fixnum value referred to by REFERENCE as a single,
atomic operation."
  (declare (ignore reference))
  (error "Atomic operations aren't supported."))

(defun atomic-decf (reference)
  "Decrements the fixnum value referred to by REFERENCE as a single,
atomic operation."
  (declare (ignore reference))
  (error "Atomic operations aren't supported."))

;;;; ugly-processes.asd

(asdf:defsystem #:ugly-processes
  :description "Library for replacing MP deps in the old libraries"
  :author "Daniel Kochmański <daniel@turtleware.eu>"
  :license "LGPL-2.1"
  :serial t
  :components ((:file "package")
               (:file "ugly-processes"))
  ;; :in-order-to ((asdf:test-op
  ;;                (asdf:test-op #:translate/test)
                 )))

#+(or)
(asdf:defsystem #:translate/test
  :depends-on (#:ugly-processes #:fiveam)
  :components ((:file "tests"))
  :perform (asdf:test-op (o s)
             (funcall (intern (string '#:run!) :ugly-processes/test)
                      :ugly-processes-tests)))
